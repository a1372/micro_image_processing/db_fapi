import os
import asyncio
from argparse import ArgumentParser

import uvicorn
from config import get_config
from api.database import create_all


parser = ArgumentParser()
parser.add_argument('--create', action='store_true')
parser.add_argument('--reload', action='store_true')        # figure this out later

args = parser.parse_args()


def create_log_dir(log_dir: str) -> None:
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)


def main() -> None:
    _config = get_config()
    create_log_dir(_config['logging']['log_dir'])
    if args.create:
        asyncio.run(create_all(_config['database']['uri']))
    else:
        uvicorn.run(
            app="api.app:app",
            log_config='logger.conf',
            **_config['uvicorn'],
        )


if __name__ == "__main__":
    main()
