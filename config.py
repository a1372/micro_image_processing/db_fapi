import os
from uuid import uuid4
from configparser import ConfigParser
from copy import deepcopy


__FILE_NAME__ = 'config.conf'
__DEFAULT_APP_CONFIG__ = {
    'database': {
        'uri': 'postgresql+asyncpg://:@localhost:5432/'
    },
    'mqtt': {
        'host': 'localhost',
        'port': 1883,
        'client_id': f"{str(uuid4())[:8]}",
        'workers_topic': 'python/image_processing'
    },
    'uvicorn': {
        'host': '0.0.0.0',
        'port': 5000,
    },
    'logging': {
        'name': 'ApiLogger',
        'config': 'logger.conf',
        'log_dir': 'logs'
    }
}

__DEFAULT_LOGGER_CONFIG__ = {
    'version': 1,
    'loggers': {
        'keys': 'root'
    },
    'handlers': {
        'keys': 'ConsoleHandler, FileHandler'
    },
    'formatters': {
        'keys': 'default'
    },
    'logger_root': {
        'level': 'DEBUG',
        'handlers': 'ConsoleHandler, FileHandler'
    },
    'handler_ConsoleHandler': {
        'class': 'StreamHandler',
        'level': 'DEBUG',
        'formatter': 'default',
        'args': '(sys.stdout,)'
    },
    'handler_FileHandler': {
        'class': 'logging.handlers.RotatingFileHandler',
        'level': 'DEBUG',
        'formatter': 'default',
    },
    'formatter_default': {
        'format': '%(levelname)-12s %(name)-12s (%(filename)-15s,%(lineno)-5d) %(asctime)s.%(msecs)03d %(message)s',
        'datefmt': '%d-%m-%Y %H:%M:%S'
    }
}


def get_config(root: str = './', path: str = None) -> dict:
    _parser = ConfigParser()

    if path:
        config_path = path
    else:
        config_path = os.path.join(root, __FILE_NAME__)
    
    if os.path.exists(config_path):
        _parser.read(config_path)
    else:
        _parser.read_dict(__DEFAULT_APP_CONFIG__)
        with open(config_path, 'w') as file:
            _parser.write(file)

    _config = _parser._sections
    _config['uvicorn']['port'] = int(_config['uvicorn']['port'])
    _config['mqtt']['port'] = int(_config['mqtt']['port'])
    return deepcopy(_config)

