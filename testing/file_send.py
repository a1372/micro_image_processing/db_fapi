import requests
import numpy as np
import cv2 as cv


URLI = 'http://127.0.0.1:5000/image/'
URLU = 'http://127.0.0.1:5000/user/'


def send_image():
    file = open('test.jpg', 'rb')
    resp = requests.post(URLI, files={'source': file})
    print(resp.status_code, resp.content)

def get_image():
    resp = requests.get(
        url=URLI,
        data={'id': 15, 'type': 'source'}
    )

    if resp.ok:
        image = cv.imdecode(np.frombuffer(resp.content, np.uint8), cv.IMREAD_COLOR)
        cv.imshow('123', image)
        cv.waitKey(0)
    else:
        print(resp.status_code, resp.content)

def get_image_data():
    resp = requests.get(
        url=URLI,
        data={'id': 15}
    )
    print(resp.status_code, resp.content)


def update_image():
    resp = requests.request(
        'patch',
        URLI,
        data={'id': 15, 'uuid': '123123123'}
    )
    print(resp.status_code, resp.content)

def update_user():
    resp = requests.request(
        'patch',
        URLU,
        data={'id': 15, 'uuid': '123123123'}
    )
    print(resp.status_code, resp.content)

def delete_image():
    resp = requests.request(
        'delete',
        URLU,
        data={'id': 12}
    )
    print(resp.status_code, resp.content)


if __name__ == '__main__':
    pass
