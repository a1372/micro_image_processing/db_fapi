class ConfigNotSetError(Exception):
    def __init__(self, msg: str = 'Application config was not set'):
        Exception.__init__(self, msg)


class FailedConnection(Exception):
    def __init__(self, msg: str = 'Failed to connect to database'):
        Exception.__init__(self, msg)


class BrokerNotConnected(Exception):
    def __init__(self, msg: str = 'Failed to connect to MQTT broker'):
        Exception.__init__(self, msg)
