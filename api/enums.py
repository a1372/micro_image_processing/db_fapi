from enum import Enum


class IType(str, Enum):
    SOURCE = 'source'
    SWAPPED = 'swapped'
    PIXELIZED = 'pixelized'


class Order(str, Enum):
    DESC = 'desc'
    ASC = 'asc'
