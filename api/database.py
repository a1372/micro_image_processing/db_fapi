from sqlalchemy import Column, Integer, String, DateTime, LargeBinary
from sqlalchemy.sql import expression
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession, AsyncEngine


__BASE__ = declarative_base()


def get_uri(user, password, db_host, database):
    return f"postgresql://{user}:{password}@{db_host}/{database}"


def get_async_engine(uri, echo=False):
    return create_async_engine(uri, echo=echo)


def get_session_maker(engine, autoflush=True, autocommit=False, expire_on_commit=False, info=None, class_=AsyncSession):
    return sessionmaker(
        bind=engine,
        autoflush=autoflush,
        autocommit=autocommit,
        expire_on_commit=expire_on_commit,
        info=info,
        class_=class_
    )


async def create_all(uri):
    engine = get_async_engine(uri=uri)
    async with engine.begin() as db:
        await db.run_sync(__BASE__.metadata.create_all)


async def drop_all(uri):
    engine = get_async_engine(uri=uri)
    async with engine.begin() as db:
        await db.run_sync(__BASE__.metadata.drop_all)


class utcnow(expression.FunctionElement):
    type = DateTime()
    inherit_cache = True


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


class Image(__BASE__):

    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    uuid = Column(String(length=255), nullable=False)
    source = Column(LargeBinary(), nullable=False)
    swapped = Column(LargeBinary(), nullable=True)
    pixelized = Column(LargeBinary(), nullable=True)
    created_at = Column(DateTime(timezone=False), nullable=False, server_default=utcnow())

    def get_data(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at
        }

    def get_files(self):
        return {
            'source': self.source,
            'swapped': self.swapped,
            'pixelized': self.pixelized
        }

class User(__BASE__):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    uuid = Column(String(length=255), nullable=False)
    user_name = Column(String(length=128), nullable=False)
    pass_hash = Column(String(length=128), nullable=False)
