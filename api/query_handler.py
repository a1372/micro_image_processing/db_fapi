from datetime import datetime as dt
from typing import Optional, List, Union

from sqlalchemy.sql import select, update, delete, and_

from api.database import *
from api.pmodels import *
from api.enums import *
from api.pmodels import pSelectImageModel, pUpdateImageModel


class QueryHandler:

    @staticmethod
    def image_filter_constructor(
        query,
        id: Optional[int] = None,
        id_list: Optional[List[int]] = None,
        id_gt: Optional[int] = None,
        id_lt: Optional[int] = None,
        uuid: Optional[str] = None,
        before: Optional[dt] = None,
        later: Optional[dt] = None,
        order: Order = Order.DESC,
        limit: Optional[int] = None
    ):
        _query = query

        if before:
            _query = _query.filter(Image.created_at<=before)

        if later:
            _query = _query.filter(Image.created_at>=later)

        if id:
            _query = _query.filter_by(id=id)
        elif id_list:
            _query = query.filter(Image.id.in_(id_list))
        else:
            if id_gt:
                _query = _query.filter_by(id>id_gt)
            if id_lt:
                _query = _query.filter_by(id>id_lt)

        if uuid:
            _query = _query.filter_by(uuid==uuid)

        if order:
            _query = _query.order_by(
                getattr(Image.id, order.value)()
            )

        if limit:
            _query = _query.limit(limit)

        return _query
    
    @staticmethod
    async def create_image(db: AsyncSession, model: pCreateImageModel) -> int:
        _image = Image(
            uuid=model.uuid,
            source=model.to_bytes()
        )
        db.add(_image)
        await db.commit()
        return _image.id

    @staticmethod
    async def select_images(db: AsyncSession, model: pSelectImageModel) -> List[Image]:
        query = QueryHandler.image_filter_constructor(
            query=select(Image),
            **model.dict()
        )

        rows = await db.execute(
            query
        )
        return rows.scalars().all()

    @staticmethod
    async def update_image(db: AsyncSession, model: pUpdateImageModel) -> int:
        values = {k: v for k, v in model.to_dict().items() if v is not None}
        values.pop('id')
        if not values:
            return 1

        _res = await db.execute(
            update(Image).values(**values).where(Image.id == model.id).returning(Image.id)
        )
        _imrow = _res.scalars().first()
        if _imrow is None:
            return 2

        await db.commit()
        return 0

    @staticmethod
    async def delete_image(db: AsyncSession, model: pSelectImageModel) -> Union[List[int], List[None]]:
        query = delete(Image)
        query = QueryHandler.image_filter_constructor(
            query=query,
            **model.dict()
        )

        _res = await db.execute(query.returning(Image.id))
        _ids = _res.scalars().all()
        await db.commit()
        return _ids

    @staticmethod
    async def create_user(db: AsyncSession, model: pUserModel) -> User:
        user = User(
            **model.dict()
        )
        db.add(user)
        await db.commit()
        return user.id

    async def select_user(db: AsyncSession, model: pUserModel) -> User:
        query = select(User)
        if model.id:
            query.filter_by(id=model.id)
        else:
            query.filter(
                and_(
                        User.user_name==model.user_name,
                        User.pass_hash==model.pass_hash
                    )
                )
        _res = await db.execute(query)
        user = _res.scalars().first()
        return user

    async def update_user(db: AsyncSession, model: pUserModel) -> int:
        values = {k: v for k, v in model.dict().items() if v is not None}
        _id = values.pop('id')
        if not values:
            return 1

        query = update(User).values(**values).where(id=_id).returning(User.id)
        _res = await db.execute(query)
        _id = _res.scalars().first()
        if _id is None:
            return 2

        await db.commit()
        return 0

    async def delete_user(db: AsyncSession, model: pUserModel) -> int:
        _res = await db.execute(
            delete(User).filter_by(id=model.id).returning(User.id)
        )
        _id = _res.scalars().first()
        if _id:
            await db.commit()
        return _id
