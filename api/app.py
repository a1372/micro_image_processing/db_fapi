import json
from uuid import uuid4
from datetime import datetime as dt, timedelta as td
from traceback import format_exc
from logging import getLogger
from typing import List, Dict
from base64 import b64encode

from sqlalchemy.sql import select
from fastapi import FastAPI, Depends, HTTPException, Response

from api.pmodels import *
from api.database import *
from api.errors import *
from api.query_handler import QueryHandler
from config import get_config


class CustomApp(FastAPI):
    __slots__ = [
        "config",
        "logger",
        "engine",
        "session_maker"
    ]

    def __init__(self):
        FastAPI.__init__(self)
        self.config             = get_config()
        self.logger             = getLogger('API')
        self.engine             = create_async_engine(self.config['database']['uri'], echo=False)
        self.session_maker      = get_session_maker(self.engine)

    def bootup(self, config, logger):
        self.config = config
        self.logger = logger

        self.engine = create_async_engine(self.config['database']['uri'])
        self.session_maker = get_session_maker(self.engine)

    async def get_connection(self) -> AsyncSession:
        async with self.session_maker() as db:
            yield db
    

app = CustomApp()


def create_uuid():
    return f"{dt.now().strftime('%Y-%m-%d')}_{uuid4()}"


def images2json(images: List[Image]) -> str:
    _r = list(
        map(
            lambda record: {
                'id': record.id,
                'uuid': record.uuid,
                'source': str(b64encode(record.source), encoding='utf-8') if record.source else None,
                'swapped': str(b64encode(record.swapped), encoding='utf-8') if record.swapped else None,
                'pixelized': str(b64encode(record.pixelized), encoding='utf-8') if record.pixelized else None,
                'created_at': record.created_at.isoformat(sep=' '),
            }, images
        )        
    )
    return json.dumps(_r)


def user2json(user: User) -> str:
    return json.dumps(
        {
            'id': user.id,
            'uuid': user.uuid,
            'user_name': user.user_name,
            'pass_hash': user.pass_hash,
        }
    )


@app.get("/")
async def root(db: AsyncSession = Depends(app.get_connection)):
    async with app.session_maker() as db:
        res = await db.execute(
            select(User)
        )
    msg = 'xdd'
    res = res.scalars().all()
    app.logger.info(msg)
    return {"msg": msg}


@app.post('/image/')
async def create_image(model: pCreateImageModel, db: AsyncSession = Depends(app.get_connection)):
    if not model.uuid:
        model.uuid = create_uuid()

    try:
        _id = await QueryHandler.create_image(model=model, db=db)
    except:
        app.logger.error(format_exc())
        raise HTTPException(500, 'Unhandled error creating image')

    app.logger.info(f'Image ({_id}) created')
    return {'id': _id}


@app.get("/image/")
async def get_image(model: pSelectImageModel, db: AsyncSession = Depends(app.get_connection)):
    images = await QueryHandler.select_images(db=db, model=model)
    return Response(
        content=images2json(images=images)
    )


@app.patch('/image/')
async def update_image(model: pUpdateImageModel, db: AsyncSession = Depends(app.get_connection)):
    _val = await QueryHandler.update_image(db=db, model=model)
    if _val == 1:
        HTTPException(422, f'No update values provided')
    elif _val == 2:
        HTTPException(404, f'Image with provided id ({model.id}) not exists')

    app.logger.info(f'Image ({model.id}) updated')
    return Response()


@app.delete('/image/')
async def delete_image(model: pSelectImageModel, db: AsyncSession = Depends(app.get_connection)):
    ids = await QueryHandler.delete_image(db=db, model=model)
    app.logger.info(f'Deleted ({len(ids)}) image rows')
    return Response(content=json.dumps(ids))


@app.post('/user/')
async def create_user(model: pUserModel, db: AsyncSession = Depends(app.get_connection)):
    if not model.uuid:
        model.uuid = create_uuid()

    try:
        _id = await QueryHandler.create_user(db=db, model=model)
    except:
        app.logger.error(format_exc())
        raise HTTPException(500, 'Unhandeled error creating user')
    app.logger.info(f'User ({_id}) created')
    return {'id': _id}


@app.get('/user/')
async def get_user(model: pUserModel, db: AsyncSession = Depends(app.get_connection)):
    if not model.id:
        if not model.user_name or not model.pass_hash:
            raise HTTPException(422, 'Invalid request body')
    user = await QueryHandler.select_user(db=db, model=model)
    return Response(user2json(user))


@app.patch('/user/')
async def update_user(model: pUserModel, db: AsyncSession = Depends(app.get_connection)):
    _val = await QueryHandler.update_user(db=db, model=model)
    if _val == 1:
        HTTPException(422, f'No update values provided')
    elif _val == 2:
        HTTPException(404, f'User with provided id ({model.id}) not exists')

    app.logger.info(f'User ({model.id}) updated')
    return Response()


@app.delete('/user/')
async def delete_user(model: pUserModel, db: AsyncSession = Depends(app.get_connection)):
    _id = await QueryHandler.delete_user(db=db, model=model)
    app.logger.info(f'User with id={_id} deleted')
    return Response(content=json.dumps({'id':_id}))
