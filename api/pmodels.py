from typing import Union, Optional, List
import base64
from datetime import datetime as dt

from pydantic import BaseModel, validator, root_validator
from api.enums import *


class pCreateImageModel(BaseModel):
    source: str
    uuid: Optional[str] = None

    def to_bytes(self):
        return base64.b64decode(bytes(self.source, encoding='utf-8'))


class pSelectImageModel(BaseModel):
    id: Optional[int] = None
    id_list: Optional[List[int]] = None
    id_gt: Optional[int] = None
    id_lt: Optional[int] = None
    uuid: Optional[str] = None
    before: Optional[dt] = None
    later: Optional[dt] = None
    order: Optional[Order] = None
    limit: Optional[int] = None


class pUpdateImageModel(BaseModel):
    id: int
    uuid: Optional[str] = None
    source: Optional[str] = None
    swapped: Optional[str] = None
    pixelized: Optional[str] = None

    def to_bytes(self) -> tuple:
        return tuple(
            map(
                lambda buff:
                    base64.b64decode(bytes(buff, encoding='utf-8')) if buff else None
                , (self.source, self.swapped, self.pixelized)
            )
        )

    def to_dict(self) -> dict():
        buffers = self.to_bytes()
        return {
            'id': self.id,
            'uuid': self.uuid,
            'source': buffers[0],
            'swapped': buffers[1],
            'pixelized': buffers[2]
        }


class pDownloadImage(BaseModel):
    id: int
    type: Optional[IType] = None


class pUserModel(BaseModel):
    id: Optional[int] = None
    uuid: Optional[str] = None
    user_name: Optional[str] = None
    pass_hash: Optional[str] = None

    @root_validator
    def check(cls, values):
        assert any(values), 'All fields cannot be None'
        return values
